#ifndef SASL_INTERFACE_H
#define SASL_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void* nds_sasl_auth;

extern int nds_sasl_initialize(const char *server, const char *plugin_path);
extern void nds_sasl_clean_up();


extern nds_sasl_auth nds_sasl_authenticate(int fd);
extern void nds_sasl_auth_clean_up(nds_sasl_auth auth);
extern const char *nds_sasl_auth_userref(nds_sasl_auth auth);

extern void nds_sasl_auth_set_server(const char *server);
extern void nds_sasl_auth_set_realm(const char *realm);
extern void nds_sasl_auth_set_fqdn(const char *fqdn);
extern void nds_sasl_auth_set_keytab(const char *keytab);
extern void nds_sasl_auth_set_krb5conf(const char *krb5conf);
extern void nds_sasl_auth_add_protocol(const char *protocol);


#ifdef __cplusplus
}
#endif


#endif // SASL_INTERFACE_H
