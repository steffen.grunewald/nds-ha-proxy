

#include "nds_sasl.h"

#include <stdlib.h>
#include <memory>

#include "buffer.hh"
#include "raw_socket.hh"
#include "auth_server.hh"

template <typename T>
class Pointer {
    Pointer(const Pointer&);
    Pointer& operator=(const Pointer&);
public:
    Pointer(): p_(0) {}
    Pointer(T* p): p_(p) {}
    T* operator->()
    {
        return p_;
    }
    T* get()
    {
        return p_;
    }
    T*
    release()
    {
        T* tmp = p_;
        p_ = 0;
        return tmp;
    }
    ~Pointer()
    {
        if (p_)
        {
            delete p_;
        }
    }
private:
    T* p_;
};



extern "C" {

int nds_sasl_initialize(const char *server, const char *plugin_path) {
    try {
        sends::sasl_server::initialize(server, plugin_path);
        return 1;
    } catch(...) {
        return 0;
    }
}

void nds_sasl_clean_up() {
    try {
        sends::sasl_server::clean_up();
    } catch(...) {}
}

nds_sasl_auth nds_sasl_authenticate(int fd) {
    try {
        sends::buffer b(2048);
        sends::raw_socket s(fd);
        Pointer<sends::auth_server> nds_auth(new sends::auth_server());
        nds_auth->authenticate(s, b);

        // everything is working, do not close the socket
        s.release();
        return reinterpret_cast<void *>(nds_auth.release());
    } catch(...) {
        return 0;
    }
}

void nds_sasl_auth_clean_up(nds_sasl_auth auth) {
    if (!auth) return;
    try {
        Pointer<sends::auth_server> p(reinterpret_cast<sends::auth_server*>(auth));
    } catch(...) {}
}

const char* nds_sasl_auth_userref(nds_sasl_auth auth) {
    if (!auth) return "";
    try {
        sends::auth_server *nds_auth = reinterpret_cast<sends::auth_server*>(auth);
        return nds_auth->refUser().c_str();
    } catch(...) {
        return "";
    }
}

void nds_sasl_auth_set_server(const char *server) {
    if (!server) return;
    try {
        sends::auth_server::refGlobal().setServer(server);
    } catch(...) {}
}

void nds_sasl_auth_set_realm(const char *realm) {
    if (!realm) return;
    try {
        sends::auth_server::refGlobal().setRealm(realm);
    } catch(...) {}
}

void nds_sasl_auth_set_fqdn(const char *fqdn) {
    if (!fqdn) return;
    try {
        sends::auth_server::refGlobal().setFQDN(fqdn);
    } catch(...) {}
}

void nds_sasl_auth_set_keytab(const char *keytab) {
    if (!keytab) return;
    ::setenv("KRB5_KTNAME", keytab, 1);
}

void nds_sasl_auth_set_krb5conf(const char *krb5conf) {
    if (!krb5conf) return;
    ::setenv("KRB5_CONFIG", krb5conf, 1);
}

void nds_sasl_auth_add_protocol(const char *protocol) {
    if (!protocol) return;
    try {
        sends::auth_server::refGlobal().addProtocol(protocol);
    } catch(...) {}
}

}
