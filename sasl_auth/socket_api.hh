/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef SENDS_SOCKET_API_HH
#define SENDS_SOCKET_API_HH

#include <inttypes.h>
#include <string>

namespace sends {

/**  The socket API defines the basic functions needed for manipulating
  *  a socket.
  *  \brief Socket API.
  *  \author J. Zweizig
  *  \version 1.0; modified April 24, 2008
  *  \ingroup sends_base
  */
class socket_api {
  public:
    /** Enumerated socket errors.
     */
    enum sock_err {
        serr_OK = 0,   ///< No error
        serr_failure,  ///< Generic error
        serr_retry,    ///< Temporary failure, try again
        serr_timeout,  ///< %Time limit expired
        serr_internal, ///< Internal error
        serr_enddata   ///< end-of-data condition
    };

    /**  Define timeout data type
      */
    typedef double wtime_type;

  public:
    /**  Construct a socket.
      *  \brief Constructor.
      */
    socket_api(void);

    /**  Close the socket ans destry the local data.
      *  \brief Destructor.
      */
    virtual ~socket_api(void);

    /**  Accept creates a new socket connected to the first external
      *  socket requesting a connection.
      *  \brief Accept a connection request.
      *  \return Pointer to a new socket connected to requestor.
      */
    virtual socket_api *accept(void) = 0;

    /**  Bind the socket to a local address. If the IP address is zero
      *  the socket will be bound to all local networks. If the port
      *  number is zero or omitted, the next available port is allocated.
      *  \brief Bind socket to a local address.
      *  \param addr Local addres to which the socet is to be bound.
      *  \return Socket error code.
      */
    virtual sock_err bind(const std::string &addr) = 0;

    /**  Test the socket (non-)blocking mode.
      *  \brief Test blocking mode.
      *  \return True if blocking.
      */
    virtual bool blocking(void) const = 0;

    /**  Abstract method to close the socket. This method will depend
      *  strongly on the implementation.
      *  \brief Close the socket.
      */
    virtual sock_err close(void) = 0;

    /**  Connect to the specified address.
      *  \brief connect to specied peer.
      *  \param s Peer socket address
      *  \return Socket error code.
      */
    virtual sock_err connect(const std::string &s) = 0;

    /**  Get the error result from the most recent socket operation.
      *  \brief get error code.
      *  \return Error code as enumerated in socket_err.
      */
    sock_err error(void) const;

    /**  Read the specified number of bytes into the buffer or until a
      *  termination character is found. The termination character is
      *  overwritten by a null and the length returned excludes the null.
      *  \brief Read a terminated data string.
      *  \param buf  Pointer to buffer to receive data.
      *  \param len  Maximum length of data to be read (in bytes).
      *  \param term termination character
      *  \return Number of data bytes read or -1 on error
      */
    virtual long gets(char *buf, long len, char term = '\n');

    /**  Perform connection handshakes. This method may be trivial
      *  in cases where no encryption or authorization takes place.
      *  \brief Handhake with connecting peer socket.
      *  \return Socket status codes.
      */
    virtual sock_err handshake(void);

    /**  Test for open socket.
      *  \brief Test socket open.
      *  \return True if socket is open
      */
    virtual bool is_open(void) const = 0;

    /**  Listen to the specified port address, Queue the specified
      *  number of connection requests pending acceptance.
      *  \brief Listen for connection requests.
      *  \param port Local port name to listen to.
      *  \param lq   Length of connection request queue
      *  \return Socket error code.
      */
    virtual sock_err listen(const std::string &port, int lq) = 0;

    /**  Parse a port address into an ip address and a port number.
      *  The input syntax is "address:port".
      *  \brief Parse an IP address.
      *  \param addr Address string.
      *  \param node IP address
      *  \param port Port number
      *  \return Socket error code.
      */
    static sock_err parse_addr(const std::string &addr, unsigned int &node,
                               unsigned short &port);

    /**  Peer name.
      */
    const char *peer_name(void) const;

    /**  Put an integer as nDigit Hexidecimal digits.
      *  @param in     Number to be written to the socket.
      *  @param nDigit Number of digits to be written to the socket.
      *  @return number of digits written to the socket or -1.
      */
    int put_hex(long in, int nDigit);

    /**  Put a 4-byte integer as nDigit Hexidecimal digits.
      *  @param in Number to be written to the socket.
      *  @return number of digits written to the socket or -1.
      */
    int put_int(int32_t in);

    /**  Read exactly the specified number of bytes into the buffer.
      *  \brief Read data.
      *  \param buf  Pointer to buffer to receive data.
      *  \param len Maximum length of data to be read (in bytes).
      *  \return Number of data bytes read or -1 on error
      */
    virtual long read(char *buf, long len);

    /**  Read up to the specified number of bytes from the socket into
      *  the buffer.
      *  \brief Read available data.
      *  \param buf  Pointer to buffer to receive data.
      *  \param len Maximum length of data to be read (in bytes).
      *  \return Number of data bytes read.
      */
    virtual long read_available(char *buf, long len) = 0;

    /**  Set the socket to blocking or non-blocking mode.
      *  \brief Set blocking mode.
      *  \param yorn New mode for blocking
      *  \return Zero on success.
      */
    virtual sock_err set_blocking(bool yorn) = 0;

    /**  Set the error code.
      *  \brief Set the error code.
      *  \param err Error code.
      *  \param where String describing error location.
      *  \return Socket error code.
      */
    sock_err set_error(sock_err err, const std::string &where = "");

    /**  Set the socket error code based on an educated guess from
      *  errno.
      *  \brief Set the error code from system codes.
      *  \param where String describing error location.
      *  \return Socket error code.
      */
    sock_err set_error(const std::string &where = "");

    /**  Test whether the socket is (still) connected.
      *  \brief test connection
      *  \return True if connected.
      */
    virtual bool test_connect(void) = 0;

    /**  Set the socket timeout for read operations. The timeout value
      *  is in seconds. A wait time of zero results in a non-blocking
      *  socket. A negative timeout results in a blocking socket.
      *  \brief Set the error code from system codes.
      *  \param timeout wait time in second, zero or negative.
      */
    void set_timeout(wtime_type timeout);

    /**  Set the verbosity level. The default level (1) prints messages
      *  for each failure.
      *  \brief Set the verbosity.
      *  \param verb Verbosity level.
      */
    void set_verbose(int verb);

    /**  Get a socket id number. In general this will be the system fd
      *  for the socket.
      *  \brief Get the socket id.
      *  \return Socket id for use in select.
      */
    virtual int socket_id(void) const = 0;

    /**  Get the system error number (errno) for the last error;
      *  \brief Get the last error number.
      *  \return System error code.
      */
    int sys_errno(void) const;

    /**  Set the verbosity level. The default level (1) prints messages
      *  for each failure.
      *  \brief Set the verbosity.
      *  \return Verbosity level.
      */
    int verbose(void) const;

    /**  Write the specified number of bytes into the buffer.
      *  \brief Write data.
      *  \param buf  Pointer to data to be written
      *  \param len Length of data to be written (in bytes).
      *  \return Number of data bytes written or -1 on error.
      */
    virtual long write(const char *buf, long len) = 0;

    /**  Wait at most the specified number of seconds for data
      *  to arrive for reading.
      *  \brief Wait for data.
      *  \param sec Maximum number of seconds to wait for data
      *  \return Integer with the following values:
      *      - -1 Systerm error occurred (e.g. EINTR)
      *      -  0 Request timed out.
      *      -  1 data available on port.
      */
    virtual int wait_data(wtime_type sec);

    /**  Where the last error took place.
      *  \brief Where the last error happened.
      *  \return Locaton string.
      */
    const std::string &where(void) const;

  protected:
    sock_err mError;     ///< Status of most recent method
    int mVerbose;        ///< Error print-out verbosity level.
    std::string mPeer;   ///< Peer name
    wtime_type mTimeout; ///< Timeout period
    int mSysError;       ///< System error code
    std::string mWhere;  ///< Error location
};

//==================================  Inline methods
inline socket_api::sock_err socket_api::error(void) const { return mError; }

inline const char *socket_api::peer_name(void) const { return mPeer.c_str(); }

inline int socket_api::sys_errno(void) const { return mSysError; }

inline int socket_api::verbose(void) const { return mVerbose; }

inline const std::string &socket_api::where(void) const { return mWhere; }

} // namespace sends

#endif // !defined(SENDS_SOCKET_API_HH)
