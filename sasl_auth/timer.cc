/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "timer.hh"

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif /* __MACH__ */

//======================================  Construct Timer, save the start time.
Timer::Timer(void) {
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    {
        clock_serv_t cclock;
        mach_timespec_t mts;

        host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
        clock_get_time(cclock, &mts);
        mach_port_deallocate(mach_task_self(), cclock);
        mStartTime.tv_sec = mts.tv_sec;
        mStartTime.tv_nsec = mts.tv_nsec;
    }
#else  /* __MACH__ */
    clock_gettime(CLOCK_REALTIME, &mStartTime);
#endif /* __MACH__ */
}

//======================================  Destroy the timer.
Timer::~Timer(void) {}

//======================================  Fetch the elapsed time.
Timer::wtime_type Timer::elapsed(void) const {
    struct timespec now;

#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    {
        int rc;
        clock_serv_t cclock;
        mach_timespec_t mts;

        host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
        rc = clock_get_time(cclock, &mts);
        mach_port_deallocate(mach_task_self(), cclock);
        if (rc != 0)
            return -1;
        now.tv_sec = mts.tv_sec;
        now.tv_nsec = mts.tv_nsec;
    }
#else /* __MACH__ */
    if (clock_gettime(CLOCK_REALTIME, &now) != 0)
        return -1;
#endif /* __MACH__ */

    return double(now.tv_sec - mStartTime.tv_sec) +
           double(now.tv_nsec - mStartTime.tv_nsec) / double(100000000);
}
