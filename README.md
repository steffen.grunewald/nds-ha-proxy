# nds-ha-proxy #
A high-availability/load balancing proxy for the LIGO NDS system.

Features:
 * Proxies NDS2 with GSSAPI or unauthenticated connections.
 * Drops backend servers that are not responding, with followup liveness checks.
 * Can act as a configuration point for a NDS cluster.
 * Can act as a simple statistics aggregator for an NDS cluster.

Build Requirements:
 * libsasl2-dev
 * go (1.15)

## Configuration ##

Reads config from a json file.

This sample config has:
 * two backend servers listed (localhost:31201, localhost:31203).
 * 2 listening endpoints
   * 1 gssapi authenticated endpoint (localhost:31200)
   * 1 unauthenticated endpoint (localhost:31202)
 * An administrative http interface (localhost:31205)
   * Serves out config information for a nds cluster.
 * Collects simple stats from a nds system, listening on a udp port.

<pre>
{
  &quot;servers&quot;: [
    {
      &quot;transport&quot;: &quot;tcp&quot;,
      &quot;address&quot;: &quot;localhost:31201&quot;,
      &quot;weight&quot;: 1
    },
    {
      &quot;transport&quot;: &quot;tcp&quot;,
      &quot;address&quot;: &quot;localhost:31203&quot;,
      &quot;weight&quot;: 1
    }
  ],
  &quot;listeners&quot;: [
    {
      &quot;auth&quot;: {
        &quot;service&quot;: &quot;nds2&quot;,
        &quot;fqdn&quot;: &quot;nds2.localhost.localdomain&quot;,
        &quot;realm&quot;: &quot;LIGO.ORG&quot;,
        &quot;mechs&quot;: [
          &quot;GSSAPI&quot;
        ],
        &quot;keytab&quot;: &quot;./keytab&quot;,
        &quot;krb5conf&quot;: &quot;./krb5.conf&quot;,
        &quot;userfile&quot;: &quot;./userfile.txt&quot;
      },
      &quot;listen&quot;: &quot;:31200&quot;,
      &quot;transport&quot;: &quot;tcp&quot;
    },
    {
      &quot;listen&quot;: &quot;:31202&quot;,
      &quot;transport&quot;: &quot;tcp&quot;
    }
  ],
  &quot;admin&quot;: {
    &quot;listen&quot;: &quot;localhost:31205&quot;,
    &quot;transport&quot;: &quot;tcp&quot;,
    &quot;io_node_cfg&quot;: {
      &quot;default&quot;: {
        &quot;network&quot;: &quot;127.0.0.0&quot;,
        &quot;port&quot;: 31200,
        &quot;memcache&quot;: &quot;memcached://localhost&quot;,
        &quot;frame_lookup&quot;: &quot;dcache://localhost:20222&quot;,
        &quot;metadata_server&quot;: &quot;nds2://localhost:31201&quot;,
        &quot;stats&quot;: &quot;&quot;,
        &quot;online&quot;: &quot;&quot;,
        &quot;remote_data_proxy&quot;: &quot;&quot;,
        &quot;concurrency&quot;: &quot;0&quot;,
        &quot;epochs&quot;: {
          &quot;ALL&quot;: [
            0,
            1999999999
          ],
          &quot;NONE&quot;: [
            0,
            0
          ]
        }
      }
    },
    &quot;nds_metadata_cfg&quot;: {
      &quot;default&quot;: {
        &quot;network&quot;: &quot;127.0.0.0&quot;,
        &quot;port&quot;: 31201
      }
    }
  },
  &quot;simple_metrics&quot;: {
    &quot;listen&quot;: &quot;localhost:31206&quot;,
    &quot;transport&quot;: &quot;udp&quot;
  },
  &quot;sasl_plugins&quot;: &quot;/usr/lib/x86_64-linux-gnu/sasl2/&quot;,
  &quot;workers&quot;: 2
}
</pre>