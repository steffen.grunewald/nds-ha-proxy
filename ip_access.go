package nds_ha_proxy

import (
	"net"
	"strings"
)

type IPAccessList struct {
	entries []net.IPNet
}

func CreateIPAccessList(inputs []string) IPAccessList {
	addressBits := []string{"/32", "/128"}
	networks := make([]net.IPNet, 0, len(inputs))
	for _, entry := range inputs {
		_, network, err := net.ParseCIDR(entry)
		if network == nil || err != nil {
			ip := net.ParseIP(entry)
			if ip == nil {
				continue
			}
			bitsIndex := 0
			if ip.To4() == nil {
				bitsIndex = 1
			}
			_, network, err = net.ParseCIDR(entry + addressBits[bitsIndex])
			if network == nil || err != nil {
				continue
			}
		}
		networks = append(networks, *network)
	}
	return IPAccessList{entries: networks}
}

func (l *IPAccessList) contains(ip net.IP) bool {
	if ip != nil {
		for _, outNet := range l.entries {
			if outNet.Contains(ip) {
				return true
			}
		}
	}
	return false
}

func (l *IPAccessList) ContainsAddress(address string) bool {
	if len(address) == 0 {
		return false
	}
	ip := net.ParseIP(address)
	if ip != nil {
		return l.contains(ip)
	}
	if strings.HasPrefix(address, "[") {
		newAddress := strings.TrimLeft(address, "[")
		ip = net.ParseIP(strings.SplitN(newAddress, "]", 2)[0])
	} else {
		ip = net.ParseIP(strings.SplitN(address, ":", 2)[0])
	}
	if ip != nil {
		return l.contains(ip)
	}
	return false
}
