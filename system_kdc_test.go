//go:build kdc_tests
// +build kdc_tests

package nds_ha_proxy

import (
	"context"
	"fmt"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"git.ligo.org/nds/nds-ha-proxy/internal/ndstest"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

/*
 This file does integration testing and uses docker/podman to run a kdc which allows authentication connections
 to be tested.

 This is not enabled by default (requiring the kdc_tests flag) as it requires a local image to have been build.

 This test will start/stop the kdc container automatically.  Just run the test.

 Currently, this can work with either Docker or Podman.  Podman is preferred if found, otherwise docker is used.
*/

type testIface interface {
	Fatal(...interface{})
	Fatalf(string, ...interface{})
	Cleanup(func())
}

var testOffsets int64
var syncTestLock sync.Mutex
var containerEngine string

func init() {
	podman := "/usr/bin/podman"
	containerEngine = "/usr/bin/docker"
	if _, err := os.Stat(podman); err == nil {
		containerEngine = podman
		fmt.Println("Podman found and will be used for tests")
	}
}

func kdcPort(offset int64) int {
	return int(offset + 2000)
}

func getOffset() int64 {
	return atomic.AddInt64(&testOffsets, 1)
}

func writeFile(filename, contents string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	_, _ = f.Write([]byte(contents))
	return nil
}

func writeUserFile(filename string, t testIface) {
	if err := writeFile(filename, "user@DEVBOX.LOCAL:0\nuser:0"); err != nil {
		t.Fatalf("Unable to create %s, %v", filename, err)
	}
}

func writeKrb5Conf(filename string, kdcPort int, t testIface) {
	contents := fmt.Sprintf(`[libdefaults]
        default_realm = DEVBOX.LOCAL

[realms]

DEVBOX.LOCAL = {
        kdc = 127.0.0.1:%d
        #admin = 127.0.0.1
        #kdc_listen = 127.0.0.1
        #kdc_tcp_listen = 127.0.0.1
}

[domain_realm]

localhost = DEVBOX.LOCAL
`, kdcPort)
	if err := writeFile(filename, contents); err != nil {
		t.Fatalf("Unable to create %s, %v", filename, err)
	}
}

func killKdc(offset int64) {
	name := fmt.Sprintf("kdc_%d", offset)
	cmd := exec.Command(containerEngine, "kill", name)
	_ = cmd.Run()
}

func startKdc(t testIface, root string, ctx context.Context, offset int64) *exec.Cmd {
	krb5Conf := path.Join(root, "krb5.conf")
	writeKrb5Conf(krb5Conf, kdcPort(offset), t)

	forward := fmt.Sprintf("%d:88/udp", kdcPort(offset))
	name := fmt.Sprintf("kdc_%d", offset)
	cmd := exec.CommandContext(ctx, containerEngine, "run", "--rm", "-v", root+":/build", "-p", forward, "--name", name, "bookworm-proxy-test-env")

	cmd.Stdout, _ = os.Create(path.Join(root, "kdc.out"))

	if err := cmd.Start(); err != nil {
		t.Fatalf("Unable to run kdc, %v", err)
	}
	t.Cleanup(func() {
		killKdc(offset)
	})
	return cmd
}

func kinit(t testIface, keytabPath string, username string) {

	cmd := exec.Command("/usr/bin/kinit", "-k", "-t", keytabPath, username)
	cmd.Stdout, _ = os.Create(path.Join(path.Dir(keytabPath), "kinit.out"))
	cmd.Stderr = cmd.Stdout
	err := cmd.Run()
	if err != nil {
		t.Fatalf("Unable to kinit, %v", err)
	}
}

func waitFor(t testIface, predicate func() bool, maxTime time.Duration) {
	timeout := time.Now().Add(maxTime)
	for !predicate() {
		if time.Now().After(timeout) {
			t.Fatalf("wait for event expired")
		}
		time.Sleep(time.Millisecond * 250)
	}
}

func getPort(address string) int {
	parts := strings.Split(address, ":")
	port, _ := strconv.ParseInt(parts[len(parts)-1], 10, 32)
	return int(port)
}

func Test_SystemIntegration(t *testing.T) {
	syncTestLock.Lock()
	defer syncTestLock.Unlock()

	offset := getOffset()
	tmpDir := t.TempDir()

	keytabDir := path.Join(tmpDir, "keytabs")
	_ = os.Mkdir(keytabDir, 0770)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	kdcCmd := startKdc(t, keytabDir, ctx, offset)
	_ = kdcCmd

	userFilePath := path.Join(keytabDir, "userfile.txt")
	writeUserFile(userFilePath, t)

	krb5CCPath := path.Join(keytabDir, "krb5cc")

	server1, err := ndstest.NewSimpleNds2Server(ctx)
	if err != nil {
		t.Fatalf("Unable to create backend simple server1, %v", err)
		return
	}
	server2, err := ndstest.NewSimpleNds2Server(ctx)
	if err != nil {
		t.Fatalf("Unable to create backend simple server2, %v", err)
		return
	}
	_ = server1
	_ = server2

	time.Sleep(time.Second * 2)
	oldCCPath := os.Getenv("KRB5CCNAME")
	defer os.Setenv("KRB5CCNAME", oldCCPath)

	os.Setenv("KRB5CCNAME", krb5CCPath)

	krb5ConfPath := path.Join(keytabDir, "krb5.conf")

	oldKrb5Conf := os.Getenv("KRB5_CONFIG")
	defer os.Setenv("KRB5_CONFIG", oldKrb5Conf)
	os.Setenv("KRB5_CONFIG", krb5ConfPath)

	kinit(t, path.Join(keytabDir, "user.keytab"), "user")

	cfg := config.Config{
		Servers: []config.ConfigServer{
			{
				Transport: "tcp",
				Address:   server1.Address(),
				Weight:    1.0,
			},
			{
				Transport: "tcp",
				Address:   server2.Address(),
				Weight:    1.0,
			},
		},
		Listeners: []config.ConfigListener{
			{
				Auth:      nil,
				Listen:    "localhost:0",
				Transport: "tcp",
			},
			{
				Auth: &config.ConfigAuth{
					ServiceName: "nds2",
					FQDN:        "localhost",
					Realm:       "DEVBOX.LOCAL",
					Mechanisms:  []string{"GSSAPI"},
					Krb5Keytab:  path.Join(keytabDir, "server.keytab"),
					Krb5Conf:    krb5ConfPath,
					Userfile:    userFilePath,
					AllowList:   []string{"127.0.0.2/32"},
				},
				Listen:    "localhost:0",
				Transport: "tcp",
			},
		},
		Admin:         nil,
		SimpleMetrics: nil,
		SaslPlugins:   "/usr/lib/x86_64-linux-gnu/sasl2",
		OSThreads:     0,
	}

	proxy := NewProxy(&cfg)
	go proxy.Run()
	defer proxy.Stop()

	waitFor(t, func() bool {
		return proxy.Running()
	},
		time.Second*2)

	unAuthPort := getPort(proxy.GetListener(0).Addr().String())
	authPort := getPort(proxy.GetListener(1).Addr().String())

	if !ndstest.SimpleConnection("localhost", unAuthPort) {
		t.Fatal("simple connection failed")
	}
	if !ndstest.SimpleConnection("localhost", authPort) {
		t.Fatal("simple auth connection failed")
	}

	if !ndstest.SimpleMultiConnection("localhost", authPort) {
		t.Fatal("simple auth connection failed")
	}

	if ndstest.SimpleUnauthConnectionWithClientNetwork("127.0.0.1", "localhost", authPort) {
		t.Fatal("simple unauth connection succeeded where ip allow lists should require sasl auth")
	}

	if !ndstest.SimpleUnauthConnectionWithClientNetwork("127.0.0.2", "localhost", authPort) {
		t.Fatal("simple unauth connection failed")
	}

}

func BenchmarkUnauthSystemIntegration(b *testing.B) {
	b.StopTimer()
	syncTestLock.Lock()
	defer syncTestLock.Unlock()

	tmpDir := b.TempDir()

	keytabDir := path.Join(tmpDir, "keytabs")
	_ = os.Mkdir(keytabDir, 0770)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	server1, err := ndstest.NewSimpleNds2Server(ctx)
	if err != nil {
		b.Fatalf("Unable to create backend simple server1, %v", err)
		return
	}
	server2, err := ndstest.NewSimpleNds2Server(ctx)
	if err != nil {
		b.Fatalf("Unable to create backend simple server2, %v", err)
		return
	}

	cfg := config.Config{
		Servers: []config.ConfigServer{
			{
				Transport: "tcp",
				Address:   server1.Address(),
				Weight:    1.0,
			},
			{
				Transport: "tcp",
				Address:   server2.Address(),
				Weight:    1.0,
			},
		},
		Listeners: []config.ConfigListener{
			{
				Auth:      nil,
				Listen:    "localhost:0",
				Transport: "tcp",
			},
		},
		Admin:         nil,
		SimpleMetrics: nil,
		OSThreads:     0,
	}

	proxy := NewProxy(&cfg)
	go proxy.Run()
	defer proxy.Stop()

	waitFor(b, func() bool {
		return proxy.Running()
	},
		time.Second*2)

	unAuthPort := getPort(proxy.GetListener(0).Addr().String())

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		if !ndstest.SimpleConnection("localhost", unAuthPort) {
			b.Fatal("simple connection failed")
		}
	}
}

func BenchmarkAuthSystemIntegration(b *testing.B) {
	b.StopTimer()
	syncTestLock.Lock()
	defer syncTestLock.Unlock()

	offset := getOffset()
	tmpDir := b.TempDir()

	keytabDir := path.Join(tmpDir, "keytabs")
	_ = os.Mkdir(keytabDir, 0770)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	kdcCmd := startKdc(b, keytabDir, ctx, offset)
	_ = kdcCmd

	userFilePath := path.Join(keytabDir, "userfile.txt")
	writeUserFile(userFilePath, b)

	krb5CCPath := path.Join(keytabDir, "krb5cc")

	server1, err := ndstest.NewSimpleNds2Server(ctx)
	if err != nil {
		b.Fatalf("Unable to create backend simple server1, %v", err)
		return
	}
	server2, err := ndstest.NewSimpleNds2Server(ctx)
	if err != nil {
		b.Fatalf("Unable to create backend simple server2, %v", err)
		return
	}
	_ = server1
	_ = server2

	time.Sleep(time.Second * 2)
	oldCCPath := os.Getenv("KRB5CCNAME")
	defer os.Setenv("KRB5CCNAME", oldCCPath)

	os.Setenv("KRB5CCNAME", krb5CCPath)

	krb5ConfPath := path.Join(keytabDir, "krb5.conf")

	oldKrb5Conf := os.Getenv("KRB5_CONFIG")
	defer os.Setenv("KRB5_CONFIG", oldKrb5Conf)
	os.Setenv("KRB5_CONFIG", krb5ConfPath)

	kinit(b, path.Join(keytabDir, "user.keytab"), "user")

	cfg := config.Config{
		Servers: []config.ConfigServer{
			{
				Transport: "tcp",
				Address:   server1.Address(),
				Weight:    1.0,
			},
			{
				Transport: "tcp",
				Address:   server2.Address(),
				Weight:    1.0,
			},
		},
		Listeners: []config.ConfigListener{
			{
				Auth: &config.ConfigAuth{
					ServiceName: "nds2",
					FQDN:        "localhost",
					Realm:       "DEVBOX.LOCAL",
					Mechanisms:  []string{"GSSAPI"},
					Krb5Keytab:  path.Join(keytabDir, "server.keytab"),
					Krb5Conf:    krb5ConfPath,
					Userfile:    userFilePath,
				},
				Listen:    "localhost:0",
				Transport: "tcp",
			},
		},
		Admin:         nil,
		SimpleMetrics: nil,
		SaslPlugins:   "/usr/lib/x86_64-linux-gnu/sasl2",
		OSThreads:     0,
	}

	proxy := NewProxy(&cfg)
	go proxy.Run()
	defer proxy.Stop()

	waitFor(b, func() bool {
		return proxy.Running()
	},
		time.Second*2)

	authPort := getPort(proxy.GetListener(0).Addr().String())

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		if !ndstest.SimpleConnection("localhost", authPort) {
			b.Fatal("simple auth connection failed")
		}
	}
}
