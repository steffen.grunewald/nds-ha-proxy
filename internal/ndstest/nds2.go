package ndstest

/*
 //#cgo pkg-config: libndscxx
 #cgo LDFLAGS: -lndscxx
 #cgo CFLAGS: -I/usr/include/nds2-client
 #cgo CXXFLAGS: -I/usr/include/nds2-client
 #include <stdlib.h>
 #include "simple_connect.h"
*/
import "C"
import (
	"fmt"
	"io"
	"net"
	"time"
	"unsafe"
)

type Krb5Config struct {
	Krb5ConfPath string
	Krb5CCPath   string
}

func SimpleConnection(host string, port int) bool {
	cHost := C.CString(host)
	defer C.free(unsafe.Pointer(cHost))

	return C.simple_connect_and_quit(cHost, C.int(port)) == C.int(1)
}

func SimpleMultiConnection(host string, port int) bool {
	cHost := C.CString(host)
	defer C.free(unsafe.Pointer(cHost))

	return C.simple_multi_connect_and_quit(cHost, C.int(port)) == C.int(1)
}

func UnauthHandshake(conn io.ReadWriter) (result bool) {
	var resultBuffer [4]byte
	result = false

	_, _ = conn.Write([]byte("authorize\n"))
	if _, err := io.ReadFull(conn, resultBuffer[:]); err != nil {
		return
	}
	if string(resultBuffer[:]) != "0000" {
		return
	}
	_, _ = conn.Write([]byte("quit;\n"))
	result = true
	return
}

func SimpleUnauthConnectionWithClientNetwork(client, host string, port int) (result bool) {
	result = false

	localAddr, err := net.ResolveTCPAddr("tcp4", client+":0")
	if err != nil {
		return false
	}
	dialer := net.Dialer{
		Timeout:   time.Millisecond * 200,
		LocalAddr: localAddr,
	}
	conn, err := dialer.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		return
	}
	defer conn.Close()
	result = UnauthHandshake(conn)
	return
}
