package nds_ha_proxy

import (
	"bufio"
	"bytes"
	"fmt"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"io"
	"log"
	"net"
	"sort"
	"strconv"
	"strings"
	"sync"
)

const (
	StatCounter = iota
	StatGage
)

/*
 * A simple metric, modelled after Statsd.
 */
type MetricValue struct {
	Type  int
	Value int64
}

type Metric struct {
	Name  string
	Value MetricValue
}

type SortMetricByName struct {
	metrics []Metric
}

func (s SortMetricByName) Len() int {
	return len(s.metrics)
}

func (s SortMetricByName) Less(i, j int) bool {
	return s.metrics[i].Name < s.metrics[j].Name
}

func (s SortMetricByName) Swap(i, j int) {
	tmp := s.metrics[i]
	s.metrics[i] = s.metrics[j]
	s.metrics[j] = tmp
}

func SortMetrics(metrics []Metric) []Metric {
	sort.Sort(SortMetricByName{metrics: metrics})
	return metrics
}

/*
 * Create a statistic that is a counter.  A counter goes up as events happen over time, thus it has history.
 */
func NewCounter(initial int64) MetricValue {
	return MetricValue{Type: StatCounter, Value: initial}
}

/*
 * Create a statistic that is a gage.  A gage is the current value of a setting
 * it can go up or down, it does not have history.
 */
func NewGage(initial int64) MetricValue {
	return MetricValue{Type: StatGage, Value: initial}
}

func NewMetricFromString(input string) Metric {
	var err error
	parts := strings.SplitN(input, "|", 2)
	valParts := strings.Split(parts[0], ":")
	val := int64(1)
	if len(valParts) > 1 {
		if val, err = strconv.ParseInt(valParts[1], 10, 64); err != nil {
			val = 0
		}
	}
	if len(parts) == 1 || parts[1] == "c" {
		return Metric{Name: valParts[0], Value: NewCounter(val)}
	}
	if parts[1] == "g" {
		return Metric{Name: valParts[0], Value: NewGage(val)}
	}
	return Metric{Name: valParts[0], Value: NewCounter(val)}
}

func (s *MetricValue) Increment(val int64) {
	s.Type = StatCounter
	s.Value += val
}

func (s *MetricValue) Set(val int64) {
	s.Value = val
}

func (s *MetricValue) MergeIn(other MetricValue) {
	if other.Type == StatGage {
		s.Type = StatGage
		s.Value = other.Value
	} else {
		if s.Type != other.Type {
			s.Type = other.Type
			s.Value = other.Value
		} else {
			s.Value += other.Value
		}
	}
}

func (s *MetricValue) String() string {
	if s.Type == StatGage {
		return fmt.Sprintf("%d (g)", s.Value)
	}
	return fmt.Sprintf("%d (c)", s.Value)
}

/* A simple stats server interface.
 */
type MetricsServer interface {
	io.Closer
	ListenAndServe()
}

type nullMetricsServer struct {
}

func (ns *nullMetricsServer) Close() error {
	return nil
}

func (ns *nullMetricsServer) ListenAndServe() {
	log.Println("Null metrics server closing")
}

type MetricsDatabase struct {
	metrics map[string]*MetricValue
	lock    sync.Mutex
}

func NewMetricsDatabase() *MetricsDatabase {
	return &MetricsDatabase{
		metrics: make(map[string]*MetricValue),
	}
}

type simpleMetricsServer struct {
	conn *net.UDPConn
	db   *MetricsDatabase
}

func (sss *simpleMetricsServer) Close() error {
	return sss.conn.Close()
}

func (sss *simpleMetricsServer) ListenAndServe() {
	log.Printf("Serving stats interface on %v", sss.conn.LocalAddr().String())
	var data [8000]byte
	for {
		count, addr, err := sss.conn.ReadFrom(data[:])
		_ = addr
		if count > 0 {
			sss.processPacket(data[0:count])
		}
		if err != nil {
			break
		}
	}
}

func (sss *simpleMetricsServer) processPacket(data []byte) {
	buffer := bytes.NewBuffer(data)
	scanner := bufio.NewScanner(buffer)

	updates := make([]Metric, 10)
	for scanner.Scan() {
		for _, key := range strings.Fields(scanner.Text()) {
			if key == "" {
				continue
			}
			m := NewMetricFromString(key)
			if m.Name != "" {
				updates = append(updates, m)
			}
		}
	}
	if len(updates) > 0 {
		sss.db.Update(updates)
	}
}

func (m *MetricsDatabase) CopyStats() []Metric {
	results := make([]Metric, 0, len(m.metrics))
	m.lock.Lock()
	defer m.lock.Unlock()
	for key, val := range m.metrics {
		results = append(results, Metric{Name: key, Value: *val})
	}
	return results
}

func (m *MetricsDatabase) Update(metrics []Metric) {
	m.lock.Lock()
	defer m.lock.Unlock()
	for _, metric := range metrics {
		if metric.Name == "" {
			continue
		}
		cur, ok := m.metrics[metric.Name]
		if ok {
			cur.MergeIn(metric.Value)
		} else {
			newMetric := &MetricValue{}
			*newMetric = metric.Value
			m.metrics[metric.Name] = newMetric
		}
	}
}

func StatsServerFactory(config *config.SimpleMetricsConfig, metrics *MetricsDatabase) MetricsServer {
	if config != nil {
		addr, err := net.ResolveUDPAddr(config.Transport, config.Listen)
		if err == nil {
			if conn, err := net.ListenUDP(config.Transport, addr); err == nil {
				return &simpleMetricsServer{
					conn: conn,
					db:   metrics,
				}
			}
		}
	}
	return &nullMetricsServer{}
}
