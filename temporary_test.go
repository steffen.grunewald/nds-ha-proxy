package nds_ha_proxy

import (
	"testing"
)

type nonTempError struct {
	Err string
}

type tempError struct {
	Err    string
	IsTemp bool
}

func (e *nonTempError) Error() string {
	return e.Err
}

func (e *nonTempError) Temporary() bool {
	return false
}

func (e *tempError) Error() string {
	return e.Err
}

func (e *tempError) Temporary() bool {
	return e.IsTemp
}

func Test_isErrorTemporary(t *testing.T) {
	type args struct {
		err error
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "nil error",
			args: args{
				err: nil,
			},
			want: false,
		},
		{
			name: "non temporary error",
			args: args{
				err: &nonTempError{Err: "something went wrong"},
			},
			want: false,
		},
		{
			name: "temporary error set to not temporary",
			args: args{
				err: &tempError{Err: "something went wrong", IsTemp: false},
			},
			want: false,
		},
		{
			name: "temporary error set to temporary",
			args: args{
				err: &tempError{Err: "try back later", IsTemp: true},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isErrorTemporary(tt.args.err); got != tt.want {
				t.Errorf("isErrorTemporary() = %v, want %v", got, tt.want)
			}
		})
	}
}
