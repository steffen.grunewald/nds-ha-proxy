package nds_ha_proxy

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"git.ligo.org/nds/nds-ha-proxy/internal/ndstest"
	"io"
	"log"
	"net"
	"reflect"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

type singleStream struct {
	data   bytes.Buffer
	lock   sync.Mutex
	signal *sync.Cond
	closed bool
}

func newSingleStream() *singleStream {
	s := &singleStream{}
	s.signal = sync.NewCond(&s.lock)
	return s
}

func (s *singleStream) Read(p []byte) (n int, err error) {
	reqSize := len(p)
	if reqSize == 0 {
		return 0, nil
	}
	s.lock.Lock()
	defer s.lock.Unlock()

	for !s.closed && s.data.Len() == 0 {
		s.signal.Wait()
	}
	if s.data.Len() > 0 {
		n, err = s.data.Read(p)
		if err != nil {
			s.closed = true
			s.signal.Broadcast()
		}
		return n, err
	}
	return 0, io.EOF
}

func (s *singleStream) Write(p []byte) (n int, err error) {
	reqSize := len(p)
	if reqSize == 0 {
		return 0, nil
	}
	s.lock.Lock()
	defer s.lock.Unlock()
	if s.closed {
		return 0, io.EOF
	}
	n, err = s.data.Write(p)
	if err != nil {
		s.closed = true
	}
	s.signal.Broadcast()
	return
}

func (s *singleStream) Close() error {
	s.lock.Lock()
	s.closed = true
	s.lock.Unlock()
	s.signal.Broadcast()
	return nil
}

func (s *singleStream) Closed() bool {
	s.lock.Lock()
	defer s.lock.Unlock()
	return s.closed
}

type dummyIO struct {
	input  *singleStream
	output *singleStream
}

func newDummyIO() *dummyIO {
	return &dummyIO{
		input:  newSingleStream(),
		output: newSingleStream(),
	}
}

func (d *dummyIO) Read(p []byte) (n int, err error) {
	return d.input.Read(p)
}

func (d *dummyIO) Write(p []byte) (n int, err error) {
	return d.output.Write(p)
}

func (d *dummyIO) Close() error {
	_ = d.input.Close()
	_ = d.output.Close()
	return nil
}

func (d *dummyIO) invertInterface() *dummyIO {
	return &dummyIO{
		input:  d.output,
		output: d.input,
	}
}

func TestRunNDS2Proxy_SendsUserInfo(t *testing.T) {

	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	var usernameSent = int64(0)
	commandTracker := func(cmd string) {
		if cmd == "__remote_user {Anonymous};" {
			atomic.StoreInt64(&usernameSent, 1)
		}
	}
	s, err := ndstest.NewSimpleNds2ServerWithCB(ctx, commandTracker)
	if err != nil {
		log.Fatalf("Unable to launch server, %v", err)
	}

	authorizer := &AnonymousAuthorizer{}
	cfg := config.Config{
		Servers: []config.ConfigServer{
			{
				Transport: "tcp",
				Address:   s.Address(),
				Weight:    1,
			},
		},
		Listeners:     nil,
		Admin:         nil,
		SimpleMetrics: nil,
		SaslPlugins:   "",
		OSThreads:     1,
	}
	servers := CreateServerList(&cfg)

	client := newDummyIO()

	w := sync.WaitGroup{}
	w.Add(1)
	handshakeOk := false
	go func() {
		defer w.Done()

		handshakeOk = ndstest.UnauthHandshake(client)
		log.Printf("handshake = %v\n", handshakeOk)
		_ = client.Close()
	}()
	_ = servers
	_ = authorizer
	RunNDS2Proxy(client.invertInterface(), servers, "label", 42, authorizer)
	w.Wait()
	if !handshakeOk {
		log.Fatal("nds handshake failed")
	}
	if atomic.LoadInt64(&usernameSent) != 1 {
		log.Fatal("username was not sent")
	}
}

func TestRunNDS2Proxy_TeardownWhenClientWaitsOnClose(t *testing.T) {

	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	commandTracker := func(cmd string) {
	}
	s, err := ndstest.NewSimpleNds2ServerWithCB(ctx, commandTracker)
	if err != nil {
		log.Fatalf("Unable to launch server, %v", err)
	}

	authorizer := &AnonymousAuthorizer{}
	cfg := config.Config{
		Servers: []config.ConfigServer{
			{
				Transport: "tcp",
				Address:   s.Address(),
				Weight:    1,
			},
		},
		Listeners:     nil,
		Admin:         nil,
		SimpleMetrics: nil,
		SaslPlugins:   "",
		OSThreads:     1,
	}
	servers := CreateServerList(&cfg)

	client := newDummyIO()

	w := sync.WaitGroup{}
	w.Add(1)
	handshakeOk := false
	go func() {
		defer w.Done()
		var result [4]byte
		handshakeOk = ndstest.UnauthHandshake(client)
		log.Printf("handshake = %v\n", handshakeOk)
		_, _ = client.Write([]byte("quit;\n"))
		_, _ = client.Read(result[:])
		_ = client.Close()
	}()
	_ = servers
	_ = authorizer
	RunNDS2Proxy(client.invertInterface(), servers, "label", 42, authorizer)
	w.Wait()
	if !handshakeOk {
		log.Fatal("nds handshake failed")
	}
}

func TestCreateServerEndpoint(t *testing.T) {
	type args struct {
		cfg config.ConfigListener
	}
	tests := []struct {
		name    string
		args    args
		want    ServerEndpoint
		wantErr bool
	}{
		{
			name: "nil mechansim list",
			args: args{
				cfg: config.ConfigListener{
					Auth: &config.ConfigAuth{
						ServiceName: "",
						FQDN:        "",
						Realm:       "",
						Mechanisms:  nil,
						Krb5Keytab:  "",
						Krb5Conf:    "",
						Userfile:    "",
					},
					Listen:    "",
					Transport: "",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty mechansim list",
			args: args{
				cfg: config.ConfigListener{
					Auth: &config.ConfigAuth{
						ServiceName: "",
						FQDN:        "",
						Realm:       "",
						Mechanisms:  []string{},
						Krb5Keytab:  "",
						Krb5Conf:    "",
						Userfile:    "",
					},
					Listen:    "",
					Transport: "",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid mechansim list",
			args: args{
				cfg: config.ConfigListener{
					Auth: &config.ConfigAuth{
						ServiceName: "",
						FQDN:        "",
						Realm:       "",
						Mechanisms:  []string{"made up mechanism"},
						Krb5Keytab:  "",
						Krb5Conf:    "",
						Userfile:    "",
					},
					Listen:    "",
					Transport: "",
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CreateServerEndpoint(tt.args.cfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateServerEndpoint() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateServerEndpoint() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func alwaysFalse() bool {
	return false
}

func alwaysTrue() bool {
	return true
}

func Test_commonRunParams_ShouldQuit(t *testing.T) {
	type fields struct {
		Transport      string
		Listen         string
		Description    string
		reportListener func(listener net.Listener)
		Ids            chan int64
		ServerList     *ServerList
		Handler        func(net.Conn, *ServerList, int64)
		shouldQuit     func() bool
		getListner     func(string, string) (net.Listener, error)
		sleep          func(time.Duration)
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name:   "default null handler",
			fields: fields{shouldQuit: nil},
			want:   false,
		},
		{
			name:   "always false",
			fields: fields{shouldQuit: alwaysFalse},
			want:   false,
		},
		{
			name:   "always true",
			fields: fields{shouldQuit: alwaysTrue},
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			params := commonRunParams{
				Transport:      tt.fields.Transport,
				Listen:         tt.fields.Listen,
				Description:    tt.fields.Description,
				reportListener: tt.fields.reportListener,
				Ids:            tt.fields.Ids,
				ServerList:     tt.fields.ServerList,
				Handler:        tt.fields.Handler,
				shouldQuit:     tt.fields.shouldQuit,
				getListener:    tt.fields.getListner,
				sleep:          tt.fields.sleep,
			}
			if got := params.ShouldQuit(); got != tt.want {
				t.Errorf("ShouldQuit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_commonRunParams_GetListener(t *testing.T) {

	handlerCalled := false

	myGetListener := func(network, address string) (net.Listener, error) {
		handlerCalled = true
		return net.Listen(network, address)
	}

	type fields struct {
		Transport      string
		Listen         string
		Description    string
		reportListener func(listener net.Listener)
		Ids            chan int64
		ServerList     *ServerList
		Handler        func(net.Conn, *ServerList, int64)
		shouldQuit     func() bool
		getListner     func(string, string) (net.Listener, error)
		sleep          func(time.Duration)
	}
	type args struct {
		network string
		address string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantErr       bool
		handlerCalled bool
	}{
		{
			name:          "default null calls net.Listener",
			fields:        fields{getListner: nil},
			args:          args{"tcp", "127.0.0.1:0"},
			wantErr:       false,
			handlerCalled: false,
		},
		{
			name:          "calls custom Listen function",
			fields:        fields{getListner: myGetListener},
			args:          args{"tcp", "127.0.0.1:0"},
			wantErr:       false,
			handlerCalled: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handlerCalled = false
			params := commonRunParams{
				Transport:      tt.fields.Transport,
				Listen:         tt.fields.Listen,
				Description:    tt.fields.Description,
				reportListener: tt.fields.reportListener,
				Ids:            tt.fields.Ids,
				ServerList:     tt.fields.ServerList,
				Handler:        tt.fields.Handler,
				shouldQuit:     tt.fields.shouldQuit,
				getListener:    tt.fields.getListner,
				sleep:          tt.fields.sleep,
			}
			got, err := params.GetListener(tt.args.network, tt.args.address)
			if got != nil {
				defer got.Close()
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListener() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil && got == nil {
				t.Errorf("GetListener() returned a nil Listener when it was not in error")
				return
			}
			if handlerCalled != tt.handlerCalled {
				t.Errorf("handlerCalled = %v, expected %v", handlerCalled, tt.handlerCalled)
				return
			}
		})
	}
}

func Test_commonRunParams_SleepFor(t *testing.T) {
	handlerCalled := false
	mySleepHandler := func(duration time.Duration) {
		handlerCalled = true
	}
	type fields struct {
		Transport      string
		Listen         string
		Description    string
		reportListener func(listener net.Listener)
		Ids            chan int64
		ServerList     *ServerList
		Handler        func(net.Conn, *ServerList, int64)
		shouldQuit     func() bool
		getListener    func(string, string) (net.Listener, error)
		sleep          func(time.Duration)
	}
	type args struct {
		duration time.Duration
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		handlerCalled bool
	}{
		{
			name:          "default nil handler",
			fields:        fields{sleep: nil},
			args:          args{time.Nanosecond * 100},
			handlerCalled: false,
		},
		{
			name:          "mock_handler",
			fields:        fields{sleep: mySleepHandler},
			args:          args{time.Nanosecond * 100},
			handlerCalled: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handlerCalled = false
			params := commonRunParams{
				Transport:      tt.fields.Transport,
				Listen:         tt.fields.Listen,
				Description:    tt.fields.Description,
				reportListener: tt.fields.reportListener,
				Ids:            tt.fields.Ids,
				ServerList:     tt.fields.ServerList,
				Handler:        tt.fields.Handler,
				shouldQuit:     tt.fields.shouldQuit,
				getListener:    tt.fields.getListener,
				sleep:          tt.fields.sleep,
			}
			params.SleepFor(tt.args.duration)
			if handlerCalled != tt.handlerCalled {
				t.Errorf("handlerCalled = %v, expected %v", handlerCalled, tt.handlerCalled)
				return
			}
		})
	}
}

type expectedDurations struct {
	expectedSleep time.Duration
	testTime      time.Duration
}

func (e expectedDurations) String() string {
	return fmt.Sprintf("[%v, %v]", e.expectedSleep, e.testTime)
}

func Test_commonRunServer_backoff(t *testing.T) {

	expectedSleeps := []expectedDurations{
		{expectedSleep: time.Second * 1, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 5, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 10, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 15, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 1, testTime: time.Second * 1},
		{expectedSleep: time.Second * 1, testTime: time.Second * 1},
		{expectedSleep: time.Second * 5, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 10, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 15, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 30, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 60, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 60, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 60, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 1, testTime: time.Second * 1},
		{expectedSleep: time.Second * 5, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 10, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 15, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 30, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 60, testTime: time.Minute * 1},
		{expectedSleep: time.Second * 60, testTime: time.Minute * 1},
	}
	sleeps := make([]time.Duration, 0, 0)

	nowValues := make([]time.Time, 0, 0)
	nowHandler := func() time.Time {
		if len(nowValues) == 0 {
			index := len(sleeps)
			nowValues = make([]time.Time, 2, 2)
			nowValues[0] = time.Now()
			nowValues[1] = nowValues[0].Add(expectedSleeps[index].testTime)
			log.Printf("Test %d duration %v", index, expectedSleeps[index].testTime)
		}
		curTime := nowValues[0]
		nowValues = nowValues[1:]
		return curTime
	}

	shouldQuit := func() bool {
		return len(sleeps) >= len(expectedSleeps)
	}
	failingListener := func(string, string) (net.Listener, error) {
		return nil, errors.New("intentionally failed Listen")
	}
	mySleep := func(duration time.Duration) {
		sleeps = append(sleeps, duration)
	}

	type args struct {
		params commonRunParams
	}
	tests := struct {
		name string
		args args
	}{

		name: "Test backoff",
		args: args{
			params: commonRunParams{
				Transport:      "tcp",
				Listen:         "127.0.0.1:0",
				Description:    "",
				reportListener: nil,
				Ids:            nil,
				ServerList:     nil,
				Handler:        nil,
				shouldQuit:     shouldQuit,
				getListener:    failingListener,
				now:            nowHandler,
				sleep:          mySleep,
			},
		},
	}

	commonRunServer(tests.args.params)

	if len(sleeps) != len(expectedSleeps) {
		t.Errorf("Unexpected number of test iterations, got %d, expected %d", len(sleeps), len(expectedSleeps))
	} else {
		for i, _ := range sleeps {
			if sleeps[i] != expectedSleeps[i].expectedSleep {
				t.Fatalf("Unexpected sleep value %d at index %d, got:\n%v\nwanted:\n%v", sleeps[i], i, sleeps, expectedSleeps)
			}
		}
	}
}

func Test_commonRunParams_Now(t *testing.T) {
	handlerCalled := false
	nowHandler := func() time.Time {
		handlerCalled = true
		return time.Now()
	}
	type fields struct {
		Transport      string
		Listen         string
		Description    string
		reportListener func(listener net.Listener)
		Ids            chan int64
		ServerList     *ServerList
		Handler        func(net.Conn, *ServerList, int64)
		shouldQuit     func() bool
		getListener    func(string, string) (net.Listener, error)
		now            func() time.Time
		sleep          func(time.Duration)
	}
	tests := []struct {
		name          string
		fields        fields
		want          time.Time
		handlerCalled bool
	}{
		{
			name: "default nil",
			fields: fields{
				now: nil,
			},
			want:          time.Now(),
			handlerCalled: false,
		},
		{
			name: "with handler",
			fields: fields{
				now: nowHandler,
			},
			want:          time.Now(),
			handlerCalled: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handlerCalled = false
			params := commonRunParams{
				Transport:      tt.fields.Transport,
				Listen:         tt.fields.Listen,
				Description:    tt.fields.Description,
				reportListener: tt.fields.reportListener,
				Ids:            tt.fields.Ids,
				ServerList:     tt.fields.ServerList,
				Handler:        tt.fields.Handler,
				shouldQuit:     tt.fields.shouldQuit,
				getListener:    tt.fields.getListener,
				now:            tt.fields.now,
				sleep:          tt.fields.sleep,
			}
			got := params.Now()
			if got.Sub(tt.want).Seconds() > 10 {
				t.Errorf("time is way out of bounds, allowing a 10s range")
			}
			if handlerCalled != tt.handlerCalled {
				t.Errorf("handlerCalled = %v, expected %v", handlerCalled, tt.handlerCalled)
				return
			}
		})
	}
}
